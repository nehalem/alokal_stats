#!/usr/bin/env python3

import json
import urllib.request as r
import locale
import calendar
from datetime import datetime
from collections import defaultdict

from tqdm import tqdm
import numpy as npy
import lxml.etree as xml
import matplotlib.pyplot as plt

TIMES = [0] * 24
POSTSBOARD = defaultdict(int)
DAYS = defaultdict(int)
JSONDATA = None
XMLDATA = None

def get_all_threads_list(res):
    urls = []
    data = xml.fromstring(res)
    for i in data.getchildren():
        urls.append(i.xpath('*/text()')[0])
    # skip boards list
    urls = urls[9:]
    return urls

def read_alokal_json_data(res):
    data = json.loads(res.decode('utf-8'))
    return data

def read_url(url):
    request = r.urlopen(url)
    response = request.read()
    request.close()
    return response

res = read_url('https://alokal.eu/sitemap.xml')
urls = get_all_threads_list(res)

def generate_posts_per_hour_graph():
    plt.title('Celkový počet plagátov na Alokali je {}'.format(sum(TIMES)))
    plt.bar(npy.arange(0, 24), TIMES, align='edge', color='red')
    plt.xticks(npy.arange(0, 24))
    plt.margins(x=0., y=0.)
    plt.xlabel("Hodziny")
    plt.ylabel('Počet odpovedí')
    plt.savefig('./alokal_posts.png', bbox_inches='tight')

def generate_posts_per_board_graph():
    plt.bar(range(len(POSTSBOARD)), list(POSTSBOARD.values()), align='edge', color='red')
    plt.xticks(range(len(POSTSBOARD)), list(POSTSBOARD.keys()))
    plt.margins(x=0., y=0.)
    plt.xlabel("Dosky")
    plt.ylabel('Počet vlákien')
    plt.savefig('./alokal_threads_per_board' + '.png', bbox_inches='tight')
    plt.clf()

def generate_number_of_posts_by_day_graph():
    plt.title('Celkový počet plagátov na Alokali je {}'.format(sum(DAYS.values())))
    plt.bar(range(7), list(DAYS.values()), align='edge', color='red')
    locale.setlocale(locale.LC_TIME, 'sk_SK')
    plt.xticks(range(7), [day for day in calendar.day_name])
    plt.margins(x=0., y=0.)
    plt.xlabel("Deň v týždni")
    plt.ylabel('Počet odpovedí')
    plt.savefig('./alokal_posts_per_day' + '.png', bbox_inches='tight')
    plt.clf()

COUNTER = 0
for thread in tqdm(urls):
    res = read_url(thread.replace('http', 'https') + '.json')
    board = thread.split('/')[3]
    POSTSBOARD[board] += 1
    JSONDATA = read_alokal_json_data(res)
    for i in JSONDATA['posts']:
        hour = datetime.fromtimestamp(i['time']).hour
        DAYS[datetime.fromtimestamp(i['time']).weekday()] += 1
        TIMES[hour] += 1
    COUNTER += 1

#generate_posts_per_board_graph()
generate_posts_per_hour_graph()
generate_number_of_posts_by_day_graph()
